import React, {useState} from 'react';
import Aux from '../Auxi/Aux';
import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

const Layout = props => {
    const [showSideDrawer, setShowSideDrawer] = useState(false);

    const sideDrawerClosedHandler = () => {
        setShowSideDrawer(false);
    };

    const sideDrawerOpenedHandler = () => {
        setShowSideDrawer(true);
    };

    return (
        <Aux>
        <Toolbar opened={sideDrawerOpenedHandler}/>
        <SideDrawer
            open={showSideDrawer}
            closed={sideDrawerClosedHandler}/>
        <main className = {classes.Content}>
            {props.children}
        </main>
        </Aux>
    )
}

export default Layout;