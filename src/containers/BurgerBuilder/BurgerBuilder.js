import Aux from '../../hoc/Auxi/Aux';
import React, { useState } from 'react';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from  '../../components/Burger/OrderSummary/OrderSummary';
// import axios from 'axios';

const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
}

const BurgerBuilder = props => {
    // state = {
    //     ingredients: {
    //         salad: 0,
    //         bacon: 0,
    //         cheese: 0,
    //         meat: 0
    //     },
    //     totalPrice: 4,
    //     purchasable: false,
    //     purchasing: false
    // }

    const [ingredients, setIngredients] = useState({
        salad: 0,
        bacon: 0,
        cheese: 0,
        meat: 0
    });
    const [totalPrice, setTotalPrice] = useState(4);
    const [purchasable, setPurchasable] = useState(false);
    const [purchasing, setPurchasing] = useState(false);

    // componentDidMount() {
    //     console.log(props);
    // }

    const updatePurchaseState = (updatedIngredients) => {
        let sum = 0;

        for (let ingredient in updatedIngredients) {
            sum += updatedIngredients[ingredient];
        }

        setPurchasable(sum > 0)
    }

    const addIngredientHandler = (type) => {
        const oldCount = ingredients[type];
        const updatedIngredients = {
            ...ingredients
        };
        updatedIngredients[type] = oldCount + 1;

        const newPrice = totalPrice + INGREDIENT_PRICES[type];
        setIngredients(updatedIngredients);
        setTotalPrice(newPrice);
        updatePurchaseState(updatedIngredients);
    }

    const removeIngredientHandler = (type) => {
        const oldCount = ingredients[type];
        const updatedIngredients = {
            ...ingredients
        };

        if (oldCount <= 0) {
            return;
        }

        updatedIngredients[type] = oldCount - 1;

        const newPrice = totalPrice - INGREDIENT_PRICES[type];
        setIngredients(updatedIngredients);
        setTotalPrice(newPrice);
        updatePurchaseState(updatedIngredients);
    }

    const purchaseHandler = () => {
        setPurchasing(true);
    }

    const purchaseCancelHandler = () => {
        setPurchasing(false);
    }

    const purchaseContinueHandler = () => {
        // const newOrder = {
        //     ingredients: this.state.ingredients,
        //     totalPrice: this.state.totalPrice
        // };

        // axios.post('http://localhost:3005/Orders', newOrder);
        // this.setState({purchasing: false});
        
        // alert('Nice! Bon appetit!');
        const queryParams = [];

        for (let i in ingredients) {
            queryParams.push(encodeURIComponent(i) + '=' + encodeURIComponent(ingredients[i]))
        }

        queryParams.push('totalPrice=' + totalPrice);
        const queryString = queryParams.join('&');

        props.history.push({
            pathname: '/checkout',
            search: '?' + queryString
        });
    }

        const disabledInfo = {
            ...ingredients
        };

        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        return (
            <Aux>
                <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
                    <OrderSummary 
                        ingredients={ingredients}
                        price={totalPrice.toFixed(2)}
                        purchaseCancelled={purchaseCancelHandler}
                        purchaseContinued={purchaseContinueHandler}/>
                </Modal>
                <Burger ingredients={ingredients}/>
                <BuildControls
                    ingredientAdded = {addIngredientHandler}
                    ingredientRemoved = {removeIngredientHandler}
                    disabled = {disabledInfo}
                    price = {totalPrice}
                    purchasable = {purchasable}
                    ordered={purchaseHandler}/>
            </Aux>
        );
}

export default BurgerBuilder;