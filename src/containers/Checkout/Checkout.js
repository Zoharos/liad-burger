import React, {useState, useEffect} from 'react';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import {Route} from 'react-router-dom';
import ContactData from './ContactData/ContactData';

const Checkout = props => {
    const [ingredients, setIngredients] = useState({});
    const [totalPrice, setTotalPrice] = useState(0);

    useEffect(() => {
        const query = new URLSearchParams(props.location.search);
        const ingredients = {};
        let price = 0;

        for (let param of query.entries()) {
            if (param[0] === 'totalPrice') {
                price = param[1];
            } else {
                ingredients[param[0]] = +param[1];
            }
        }

        setIngredients(ingredients);
        setTotalPrice(price);
    }, []);

    const checkoutCancelledHandler = () => {
        props.history.goBack();
    }

    const checkoutContinuedHandler = () => {
        props.history.replace('/checkout/contact-data');
    }

    return (
        <div>
            <CheckoutSummary 
                ingredients={ingredients}
                checkoutCancelled={checkoutCancelledHandler}
                checkoutContinued={checkoutContinuedHandler}/>
            <Route path={props.match.path + '/contact-data'}
                    render={(props) => (<ContactData
                                    ingredients={ingredients}
                                    totalPrice={totalPrice}/>)}
                                    // {...this.props}
                                    />
        </div>
    );
}

export default Checkout;