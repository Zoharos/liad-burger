import React, { useState, useEffect } from "react";
import Order from '../../components/Order/Order';
import axios from 'axios';

const Orders = props => {
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:3005/Orders')
            .then(resp => {
                const fetchedOrders = [];
                const orders = resp.data;

                for (let order in orders) {
                    fetchedOrders.push({
                        ...resp.data[order],
                        id: order
                    })
                }

                setOrders(fetchedOrders);
            });
    }, []);

    return (
        <div>
            {orders.map(order => (
                <Order 
                    key={order.id}
                    ingredients={order.ingredients}
                    totalPrice={order.totalPrice}/>
            ))}
        </div>
    );
}

export default Orders;