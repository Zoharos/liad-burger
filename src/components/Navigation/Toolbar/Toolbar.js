import React from 'react';
import classes from './Toolbar.module.css';
import classesDrawer from './DrawerToggle.module.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';

const toolbar = (props) => (
    <header className={classes.Toolbar}>
        {/* <div onClick={props.opened}>MENU</div> */}
        <div className={classesDrawer.DrawerToggle} onClick={props.opened}>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <Logo height="80%"/>
        <nav className={classes.DesktopOnly}>
            <NavigationItems/>
        </nav>
    </header>
);

export default toolbar;