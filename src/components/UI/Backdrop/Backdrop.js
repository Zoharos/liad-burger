import React from 'react';
import classes from './Backdrop.module.css';

const backdorp = (props) => (
    props.show ? <div className={classes.Backdrop} onClick={props.clicked}></div> : null
);

export default backdorp;